#!/usr/bin/env python2

import executive_engine_api as api
import rospy
'''
This is a simple mission, the drone takes off, follows a path and lands
There is a little detail in the way of activating the behaviors:
1.activateBehavior. Activates the behavior and let other behaviors run.
2.executeBehavior. Activates the behavior and do not let other behaviors run.
'''
def runMission():
  print("Starting mission...")
  print("Taking off...")
  api.activateBehavior('TAKE_OFF')

  print("Following path")

  api.activateBehavior('FOLLOW_PATH_WITH_PID_CONTROL', path=[ [0, 2, 1] , [2, 2, 1] , [2, 0, 1] , [0, 0, 1] ])

  print("Landing")

  result = api.activateBehavior('LAND')
  print('Finish mission...')