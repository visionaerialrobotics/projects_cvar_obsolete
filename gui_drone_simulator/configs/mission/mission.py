#!/usr/bin/env python2

import executive_engine_api as api
import rospy

def runMission():
  print("Starting mission...")

  print("Taking off...")
  api.executeBehavior('TAKE_OFF')

  print("Following path...")
  api.executeBehavior('FOLLOW_PATH_WITH_PID_CONTROL', path=[ [0, 4, 1] , [4, 4, 1] ])

  print("Following path...")
  api.executeBehavior('FOLLOW_PATH_WITH_PID_CONTROL', path=[ [4, 0, 1] , [0, 4, 1] ])

  print("Rotating...")
  api.executeBehavior('ROTATE_WITH_PID_CONTROL', angle= 179)

  print("Landing...")
  api.executeBehavior('LAND')

  print('Mission Finished')
