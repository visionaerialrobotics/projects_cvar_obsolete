#!/bin/bash

NUMID_DRONE=111
DRONE_SWARM_ID=1
DRONE_IP=192.168.1.1
export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects/gui_drone_simulator

. ${AEROSTACK_STACK}/setup.sh
OPEN_ROSCORE=1



gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Coordinator                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Coordinator" --command "bash -c \"
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  behavior_catalog_path:=${AEROSTACK_PROJECT}/configs/mission/behavior_catalog.yaml \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Tree Editor                                                                        ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Tree Editor" --command "bash -c \"
roslaunch behavior_tree_editor behavior_tree_editor.launch --wait \
  robot_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT} \
  mission_config_path:=${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE;
exec bash\""  &


