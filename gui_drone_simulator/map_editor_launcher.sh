#!/bin/bash

NUMID_DRONE=111
DRONE_SWARM_ID=1
DRONE_IP=192.168.1.1
export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects/gui_drone_simulator

. ${AEROSTACK_STACK}/setup.sh
OPEN_ROSCORE=1



gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Environment Map  Editor                                                                     ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Environment Map  Editor" --command "bash -c \"
roslaunch environment_editor environment_editor.launch --wait \
  robot_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT} \
  robot_config_path:=${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE;
exec bash\""  &


